#define MAX_BYTES 255
#define INITIAL_CAPACITY 37560

typedef struct {
	int32_t ord; //ordem do arquivo original
	int32_t n_bytes; // n byte em um vetor
	char bytes[MAX_BYTES]; // vetor com os bytes
}Package;


typedef struct {
	unsigned long int length;
	Package **p;
} ArrayP;
