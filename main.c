#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <dirent.h>
#include <string.h>
#include <unistd.h>
#include "filedef.h"
#define LENGTH(x)  (sizeof(x) / sizeof((x)[0]))

Package *readFile(char filename[]);
ArrayP *readDirectory(char baseDirectory[]);
int compare(const void *s1, const void *s2);
int countFiles(char *baseDirectory) ;
int toInt(char *string);

int main() {
	FILE *fs1, *fs2, *fs3, *fs4, *ft;
	int i;
	char ch, buff[1024];
	char *cwd = getcwd(buff, sizeof(buff));
	Package* p;
	ArrayP* arr = readDirectory(strcat(cwd, "/files"));
	for (int i = 0; i < arr->length; i++) {
		p = arr->p[i];
		puts(p->n_bytes);
	}
//	Package info[] = { { 4, "/home/ellen/Documentos/newad" }, { 2,
//			"/home/ellen/Documentos/newab" }, { 1,
//			"/home/ellen/Documentos/newaa" }, { 3,
//			"/home/ellen/Documentos/newac" } };
//
//	qsort(info, 4, sizeof(Package), compare);
//
//	fs1 = fopen(info[0].n, "r");
//	fs2 = fopen(info[1].n, "r");
//	fs3 = fopen(info[2].n, "r");
//	fs4 = fopen(info[3].n, "r");
//
//	ft = fopen("/home/ellen/Documentos/newzip.zip", "w");
//
//	while ((ch = fgetc(fs1)) != EOF)
//		fputc(ch, ft);
//
//	while ((ch = fgetc(fs2)) != EOF)
//		fputc(ch, ft);
//
//	while ((ch = fgetc(fs3)) != EOF)
//		fputc(ch, ft);
//
//	while ((ch = fgetc(fs4)) != EOF)
//		fputc(ch, ft);
//
//	printf("Quatro arquivos concatenados.\n", file5);
//
//	fclose(fs1);
//	fclose(fs2);
//	fclose(fs3);
//	fclose(fs4);
//	fclose(ft);

	return 0;
}
int compare(const void *s1, const void *s2) {
	Package *e1 = (Package *) s1;
	Package *e2 = (Package *) s2;
	return e1->ord - e2->ord;
}

Package *readFile(char filename[]) {
	int length = LENGTH(filename);
	if (length > 4) {
		filename[length - 5] = (char) 0;
	}
	int i = toInt(filename);
	Package *p = malloc(sizeof(Package));
	return p;
}

ArrayP *readDirectory(char *baseDirectory) {

	printf("Read directory %s\n\n", baseDirectory);
	ArrayP* result = malloc(sizeof(ArrayP));
	Package* tmp;
	DIR *dp;

	dp = opendir(baseDirectory);

	if (dp != NULL) {
		struct dirent *ep;
		int dirSize = countFiles(baseDirectory);
		printf("Size: %d\n", dirSize);
		result->p = malloc(sizeof(Package) * dirSize);
		unsigned int ri = 0;
		while (ep = readdir(dp)) {
			printf("Lendo diretório: %s \n", ep->d_name);
			tmp = readFile(ep->d_name);
			result->p[ri] = tmp;
			ri++;
		}
		result->length = ri;
		(void) closedir(dp);

	} else
		printf("Couldn't open the directory");

	return result;

}
int countFiles(char *baseDirectory) {
	DIR *dp = opendir(baseDirectory);
	struct dirent *ep;
	int dirSize = 0;
	while ((ep = readdir(dp))) {
		++dirSize;
	}
	(void) closedir(dp);
}
int toInt(char *string) {
	int num;
	sscanf(string, "%d", &num);
	return num;
}
